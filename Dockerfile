FROM node:22-alpine

WORKDIR /app

COPY . .

RUN npm install

EXPOSE 3100

CMD ["node", "server.js"]