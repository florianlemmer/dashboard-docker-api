const express = require('express');
const Docker = require('dockerode');
const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const fsExtra = require('fs-extra');
require('dotenv').config();

const app = express();
const docker = new Docker();
const networkName = 'dungeon';
const configFileName = 'playerConfig.json';
const PORT = process.env.PORT;
const createdDockerContainers = [];
const imageNames = [
  'registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/player-hackschnitzel:latest',
  'registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/player-thelegend27:latest',
  //'registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/player-monte:latest'
];

//hier origins angeben, die http-requests senden dürfen wegen CORS-issue
app.use(cors({
    origin: 'http://localhost:4200'
  }));

app.use(bodyParser.json());

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      const tmpDir = path.join(__dirname, 'tmp/');
        
      if (!fs.existsSync(tmpDir)) {
          fs.mkdirSync(tmpDir);
      }

      if (file.originalname === configFileName) {
        cb(null, path.join(__dirname, 'tmp/'));
      }else {
        req.fileValidationError = 'Die hochgeladene Datei hat nicht den erwarteten Namen.';
        cb(null, "");
    }
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });


//startet Container zusammen mit den in der Config-File übergebenen Werte als Environment-Variablen
app.post('/docker/configureAndRunWithUpload', upload.any(), async (req, res) => {
  try {
      if (req.fileValidationError) {
        return res.status(400).json({ message: req.fileValidationError });
      }
    
      const {imageName, port, containerName } = req.body;
      const envPathInImage = `/app/${configFileName}`;
      const envFile = req.files.find(file => file.originalname === configFileName);

      if (!imageName || !port || !containerName || !envFile) {
        return res.status(400).json({ message: 'Fehlende Parameter: imageName, port, containerName oder configFileName' });
      }
    
      const newFolderPath = path.join(__dirname, `./customPlayerConfigurations/${containerName}`); 
      if (!fs.existsSync(newFolderPath)) {
          fs.mkdirSync(newFolderPath, { recursive: true });
          console.log('Neuer Ordner wurde erfolgreich erstellt.');
      } else {
          console.log('Der Ordner existiert bereits.');
      }

      const newConfigFilePath = path.join(newFolderPath, configFileName);
      fs.renameSync(envFile.path, newConfigFilePath);
      console.log('Konfigurationsdatei erfolgreich verschoben:', newConfigFilePath);

      let configVariables=[];
      await readEnvVariablesFromJSON(newConfigFilePath)
        .then(envVariables => {
            console.log('Umgebungsvariablen aus der JSON-Datei:', envVariables);
            configVariables = envVariables;
        })
        .catch(error => {
            console.error(error);
            return res.status(500).json({ message: 'Ein Fehler beim Startes des Containers ist aufgetreten, weil File nicht gelesen werden konnte: '+ err.message});
        });
 
      await pullImageIfNotExists(imageName);

      const containerConfig = {
          Image: imageName,
          name: containerName,
          HostConfig: {
              PortBindings: {
                  [`${port}/tcp`]: [{ HostPort: `${port}` }]
              },
               
          },
          Env:configVariables,
          NetworkingConfig: {
              EndpointsConfig: {
                  [networkName]: {}
              }
          }
      };

      const container = await docker.createContainer(containerConfig);
      createdDockerContainers.push(containerName);
      await container.start();

      res.json({ 
          message: 'Container mit Konfiguration erfolgreich gestartet',
          containerName: containerName,
      });
  } catch (err) {
      console.error('Fehler beim Starten des Containers mit Konfiguration:', err);
      return res.status(500).json({ message: 'Ein Fehler beim Startes des Containers ist aufgetreten: '+ err.message});
    
  }
});

app.post('/docker/stop', async (req, res) => {
    try {
        const { containerName } = req.body;
        const container = docker.getContainer(containerName);
        await container.stop();
        res.json({ 
          message: 'Container erfolgreich gestoppt',
          containerName: containerName,
        });


        const configFilePath = path.join(__dirname, `customPlayerConfigurations/${containerName}`); 
        fsExtra.removeSync(configFilePath);
        console.log('Config-Ordner erfolgreich gelöscht:');
        

    } catch (err) {
        console.error('Fehler beim Stoppen des Containers:', err);
        res.status(500).send('Ein Fehler beim Stoppen des Containers ist aufgetreten');
    }
});

app.post('/docker/stopAndRemove', async (req, res) => {
  try {
      const { containerName } = req.body;
      const container = docker.getContainer(containerName);

      const containerInfo = await container.inspect();
      if (containerInfo.State.Status === 'running') {
        await container.stop();
      }

      await container.remove();

      const configFilePath = path.join(__dirname, `customPlayerConfigurations/${containerName}`); 
      fsExtra.removeSync(configFilePath);

      let index = createdDockerContainers.indexOf(containerName);
      if (index > -1) {
        createdDockerContainers.splice(index, 1);
      }

      res.json({ 
        message: 'Container erfolgreich gestoppt und gelöscht',
        containerName: containerName,
      });
  } catch (err) {
      console.error('Fehler beim Stoppen und Löschen des Containers:', err);
      res.status(500).send('Ein Fehler beim Stoppen oder Löschen des Containers ist aufgetreten');
  }
});

app.post('/docker/remove', async (req, res) => {
  try {
      const { containerName } = req.body;
      const container = docker.getContainer(containerName);

      await container.remove();

      const configFilePath = path.join(__dirname, `customPlayerConfigurations/${containerName}`); 
      fsExtra.removeSync(configFilePath);

      let index = createdDockerContainers.indexOf(valueToRemove);
      if (index > -1) {
        createdDockerContainers.splice(index, 1);
      }

      res.json({ 
        message: 'Container erfolgreich gelöscht',
        containerName: containerName,
      });
  } catch (err) {
      console.error('Fehler beim Löschen des Containers:', err);
      res.status(500).send('Ein Fehler ist aufgetreten');
  }
});

function readEnvVariablesFromJSON(jsonFilePath) {
  return new Promise((resolve, reject) => {
      fs.readFile(jsonFilePath, 'utf8', (err, data) => {
          if (err) {
              reject('Fehler beim Lesen der JSON-Datei: ' + err);
              return;
          }

          try {
              const jsonObj = JSON.parse(data);
              const envVariables = [];
              for (const key in jsonObj) {
                  if (jsonObj.hasOwnProperty(key)) {
                      envVariables.push(`${key}=${jsonObj[key]}`);
                  }
              }
              resolve(envVariables);
          } catch (parseError) {
              reject('Fehler beim Parsen der JSON-Datei: ' + parseError);
          }
      });
  });
}

async function pullImageIfNotExists(image) {
  const imageName = image.split(':')[0];
  const imageTag = image.split(':')[1] || 'latest';

  const images = await docker.listImages();
  const existingImage = images.find(img => img.RepoTags.includes(image));

  if (!existingImage) {
    console.log(`Image ${image} not found. Pulling...`);
    await pullImage(image);
    console.log(`Image ${image} pulled successfully.`);
  } else {
      console.log(`Image ${image} exists.`);
  }
}

async function pullImage(image) {
  return new Promise((resolve, reject) => {
    docker.pull(image, (err, stream) => {
      if (err) return reject(err);
      docker.modem.followProgress(stream, (err, res) => err ? reject(err) : resolve(res));
    });
  });
}

async function init() {
  try {
    for (const image of imageNames) {
      await pullImageIfNotExists(image);
    }
  } catch (err) {
    console.error('Error pulling images:', err);
  }
}

async function cleanup() {
  for (const containerName of createdDockerContainers) {
    try {
      const container = docker.getContainer(containerName);
      const containerInfo = await container.inspect();
      if (containerInfo.State.Status === 'running') {
        await container.stop();
      }

      const configFilePath = path.join(__dirname, `customPlayerConfigurations/${containerName}`); 
      fsExtra.removeSync(configFilePath);

      await container.remove();
      console.log(`Container ${containerName} stopped and removed`);
    } catch (error) {
      console.error(`Error stopping/removing container ${containerName}: ${error.message}`);
    }
  }
  process.exit(0);
}


app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});

init();

process.on('SIGINT', cleanup);
process.on('SIGTERM', cleanup);

