# MSD-Dashboard-Docker-Api

The API is closely tied to the MSD Dashboard and is used to start, stop, and delete predefined players as Docker containers using a given configuration.

The API offers various endpoints:

- `/docker/configureAndRunWithUpload`: Requires a file named playerConfig.json, containing a single Json with variables which will be set as environment variables in the Docker container, as well as an image name, port, and container name. The API creates a container with these configurations.
- `/docker/stop`: Stops the Docker container with the provided name.
- `/docker/stopAndRemove`: Stops and deletes the Docker container with the provided name.
- `/docker/remove`: Deletes the Docker container with the provided name.

When the API is terminated, all Docker containers created by the API are automatically stopped and deleted.

Note: At startup, the API pulls the images for the standard players if they are not already available, as these are necessary for a specific Dashboard feature.

## Local Installation 

Step 1: Clone the repository:
```
git clone https://gitlab.com/florianlemmer/dashboard-docker-api
```

Step 2: Install the dependencies:
```
npm install
```

Step 3: Run the project:
```
npm start
```
Alternatively:
```
node server.js
```

The API will then be accessible at `localhost:3100`.